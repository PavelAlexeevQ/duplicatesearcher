import sys
import os
import time

file_full_path_field = 'file_full_path'
file_size_field = 'file_size'

duplicated_files = []


def collect_files():
    search_path = '.'
    if len(sys.argv) == 2:
        print('Argument :', sys.argv[1])
        search_path = sys.argv[1]
    print(search_path)

    if not os.path.isdir(search_path):
        print(f'{search_path} is not a directory')
        exit(1)

    files_list = []
    for (dirpath, dirnames, filenames) in os.walk(search_path):
        for file in filenames:
            file_full_path = os.path.join(dirpath, file)
            try:
                file_size = os.path.getsize(file_full_path)
            except:
                print(f'Cannot open file {file_full_path}')
                continue
            files_list.append({file_full_path_field: file_full_path, file_size_field: file_size})

    return files_list


def check_duplicates_with_same_size(file_list, file_size):
    # since we can not open all files at once (because of OS limitation, lets open them one by one)
    # calculate hash for whole file must be very expensive, for example we may found the very first byte is different,
    # so lets calculate hash chunk by chunk
    offset = 0
    # lets check compare first 1k of files, than compare by bigger pieces
    max_chunk_size = 1024
    grouped_duplicated_files = [file_list]
    while offset < file_size:
        new_grouped_duplicated_files = []
        chunk_size = min(file_size - offset, max_chunk_size)
        for file_group in grouped_duplicated_files:
            hashed_files = {}
            for file_name in file_group:
                bin_file = None
                try:
                    bin_file = open(file_name, 'rb')
                except:
                    print(f'Cannot open {file_name}')
                    continue
                bin_file.seek(offset)
                chunk = bin_file.read(chunk_size)
                if chunk not in hashed_files:
                    hashed_files[chunk] = []
                hashed_files[chunk].append(file_name)

            for fg in hashed_files.values():
                if len(fg) > 1:
                    new_grouped_duplicated_files.append(fg)

        offset = offset + chunk_size
        # #increase piece size to speedup
        max_chunk_size = 8388608 # 8M
        grouped_duplicated_files = new_grouped_duplicated_files

    return grouped_duplicated_files

def find_duplicates(file_list):
# step 2 - group all files by size
    file_list.sort(key=lambda x: x[file_size_field])
    total_size = 0
    file_list_grouped = {}
    for file in file_list:
        file_size = file[file_size_field]
        if file_size == 0:
            continue
        if file_size not in file_list_grouped:
            file_list_grouped[file_size] = []
        file_list_grouped[file_size].append(file[file_full_path_field])
        total_size += file_size
    print(f'Total files: {len(file_list)} with size: {total_size}')

# step 3 - take files with same size and find duplicates
    all_duplicates = []
    for files_group in file_list_grouped.items():
        if len(files_group[1]) == 1:
            continue

        file_size = files_group[0]
        file_list = files_group[1]
        duplicates_same_size = check_duplicates_with_same_size(file_list=file_list, file_size=file_size)
        if len(duplicates_same_size) > 0:
            all_duplicates.append(duplicates_same_size)

    return all_duplicates


def current_milli_time():
    return round(time.time() * 1000)


# main function
start_time = current_milli_time()
# step 1 - collect all files and their sizes
all_file_list = collect_files()
duplicated_result = find_duplicates(all_file_list)
finish_time = current_milli_time()

total_duplicated_files = 0
for file_group_by_size in duplicated_result:
    print('<<<<<')
    file_group_by_size.sort()
    for file_group_dup in file_group_by_size:
        for file in file_group_dup:
            print(file)
        total_duplicated_files += 1
    print('>>>>>')

print(f'duplicates: {total_duplicated_files}')
print(f'total time {(finish_time - start_time) / 1000.0}')
